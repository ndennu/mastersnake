package com.bltnpnd.mastersnake.module

import com.bltnpnd.mastersnake.models.Grid
import com.bltnpnd.mastersnake.models.GridContent
import com.bltnpnd.mastersnake.models.Snake
import kotlin.random.Random

class Fruit {

    companion object {
        private var instance: Fruit? = null

        fun getInstance(): Fruit {
            if (instance == null) {
                instance = Fruit()
            }
            return instance!!
        }
    }

    fun fruitManagement(grid: Grid) {
        if(grid.containFruit())
            return

        val emptyList = grid.getEmptyBox()
        val index = Random.nextInt(0, emptyList.size - 1)
        val box = emptyList[index]
        box.content = GridContent.FRUIT

        grid.updateBox(box)
    }

    fun isEat(grid: Grid, snake: Snake): Boolean {
        return grid.getBoxContentByCoordinate(snake.x, snake.y) == GridContent.FRUIT
    }
}