package com.bltnpnd.mastersnake.module

import android.util.Log
import com.bltnpnd.mastersnake.GameEngine
import com.bltnpnd.mastersnake.models.*
import com.bltnpnd.mastersnake.models.Snake

class Movement {

    private var oldSnakeHead: Snake? = null

    companion object {
        private var instance: Movement? = null

        fun getInstance(): Movement {
            if (instance == null) {
                instance = Movement()
            }
            return instance!!
        }
    }

    fun moveRight(grid: Grid, snake: Snake): Boolean {
        oldSnakeHead = snake.cloneSnake()
        snake.move(Direction.RIGHT)
        return updateGrid(grid, snake, Direction.RIGHT)
    }

    fun moveLeft(grid: Grid, snakeHead: Snake): Boolean {
        oldSnakeHead = snakeHead.cloneSnake()
        snakeHead.move(Direction.LEFT)
        return updateGrid(grid, snakeHead, Direction.LEFT)
    }

    fun moveDown(grid: Grid, snakeHead: Snake): Boolean {
        oldSnakeHead = snakeHead.cloneSnake()
        snakeHead.move(Direction.DOWN)
        return updateGrid(grid, snakeHead, Direction.DOWN)
    }

    fun moveUp(grid: Grid, snakeHead: Snake): Boolean {
        oldSnakeHead = snakeHead.cloneSnake()
        snakeHead.move(Direction.UP)
        return updateGrid(grid, snakeHead, Direction.UP)
    }

    private fun updateGrid(grid: Grid, snake: Snake, direction: Direction): Boolean {

        if(snake.x < 0){
            snake.x = grid.width - 1
        } else if(snake.x == grid.width){
            snake.x = 0
        }

        if(snake.y < 0){
            snake.y = grid.height - 1
        } else if(snake.y == grid.height){
            snake.y = 0
        }

        if (Fruit.getInstance().isEat(grid, snake)) {
            snake.addPart(direction)
            GameEngine.SCORE += 10
        }

        if (snake.biteHimself()) {
            return false
        }

        /*when(grid.getBoxContentByCoordinate(snake.x, snake.y)){
            GridContent.SNAKE -> {
                Log.e("TODO", "TODO")
                // TODO: Must return false if you loose. Currently its fail when you eat a fruit
            }
            GridContent.FRUIT -> snake.addPart(direction)
        }*/

        grid.updateSnake(snake, oldSnakeHead!!)



        return true
    }
}
