package com.bltnpnd.mastersnake

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import kotlinx.android.synthetic.main.activity_pause.*

class PauseActivity : AppCompatActivity() {

    companion object {
        const val CONTINUE_RESULT: Int = 1511
        const val RESTART_RESULT: Int = 1512
        const val QUIT_RESULT: Int = 1513
        const val SCORE: String = "score"
        const val IS_GAME_OVER: String = "isGameOver"
        const val GAME_OVER_TITLE = "Game Over"
        const val PAUSE_TITLE = "Pause"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pause)

        setPopupType()
        setScore()

        restart_button.setOnClickListener {
            setResult(RESTART_RESULT)
            finishAfterTransition()
        }

        cancel_button.setOnClickListener {
            setResult(CONTINUE_RESULT)
            finishAfterTransition()
        }

        quit_button.setOnClickListener {
            setResult(QUIT_RESULT)
            finishAfterTransition()
        }
    }

    private fun setPopupType() {
        val type : Boolean = intent.getBooleanExtra(IS_GAME_OVER, false)
        if (type) {
            title_popup.text = GAME_OVER_TITLE
            cancel_button.visibility = View.GONE
        } else {
            title_popup.text = PAUSE_TITLE
            cancel_button.visibility = View.VISIBLE
        }

    }

    private fun setScore() {
        val score : Int = intent.getIntExtra(SCORE, 0)
        score_txt.text = "Score : $score"
    }
}
