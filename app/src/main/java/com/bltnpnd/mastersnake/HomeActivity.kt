package com.bltnpnd.mastersnake

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_home.*

class HomeActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        initUI()
    }

    private fun initUI() {
        play_button.setOnClickListener {
            Toast.makeText(this, "PLAY", Toast.LENGTH_SHORT).show()
            Intent(this, GameActivity::class.java)
                .also {intent -> startActivity(intent)}
        }

        scores_button.setOnClickListener {
            Intent(this, ScoresActivity::class.java)
                .also { intent -> startActivity(intent) }
        }
    }
}
