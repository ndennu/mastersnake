package com.bltnpnd.mastersnake

import com.bltnpnd.mastersnake.models.Box

interface IGameView {
    fun update(list: ArrayList<Box>, score: Int)
    fun pause()
    fun gameOver()
    fun initUI()
}