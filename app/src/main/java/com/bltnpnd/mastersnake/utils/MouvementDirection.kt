package com.bltnpnd.mastersnake.utils

import com.bltnpnd.mastersnake.models.Direction

class MouvementDirection {

    companion object {
        fun isOpposite(a: Direction, b: Direction): Boolean {
            if ((a == Direction.UP && b == Direction.DOWN) || (a == Direction.DOWN && b == Direction.UP))
                return true
            else if ((a == Direction.LEFT && b == Direction.RIGHT) || a == Direction.RIGHT && b == Direction.LEFT)
                return true
            return false
        }

        fun getOpposite(direction: Direction): Direction {
            return when(direction) {
                Direction.UP -> Direction.DOWN
                Direction.DOWN -> Direction.UP
                Direction.LEFT -> Direction.RIGHT
                Direction.RIGHT -> Direction.LEFT
            }
        }
    }
}
