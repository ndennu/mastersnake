package com.bltnpnd.mastersnake


import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.support.annotation.RequiresApi
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.AppCompatImageView
import android.util.Log
import android.view.View
import android.widget.ImageView
import android.widget.TableLayout
import android.widget.TableRow
import com.bltnpnd.mastersnake.models.Box
import com.bltnpnd.mastersnake.models.Direction
import com.bltnpnd.mastersnake.models.GridContent
import com.bltnpnd.mastersnake.models.Score
import com.bltnpnd.mastersnake.sqlite.SqlManagement
import kotlinx.android.synthetic.main.activity_game.*
import kotlinx.android.synthetic.main.header_game_activity.*
import java.sql.Date
import java.text.SimpleDateFormat
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

class GameActivity : AppCompatActivity(), IGameView {

    private lateinit var gameEngine: GameEngine
    private var currentScore = 0
    private var isGameOver = false

    companion object {
        const val PAUSE_RESULT: Int = 1504
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_game)

        gameEngine = GameEngine(this)
        setupUI()
    }

    override fun onRestart() {
        super.onRestart()
        gameEngine.pause()
    }

    override fun onDestroy() {
        super.onDestroy()
        gameEngine.kill()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == PAUSE_RESULT) {
            when (resultCode) {
                PauseActivity.CONTINUE_RESULT -> {
                    gameEngine.resume()
                }
                PauseActivity.RESTART_RESULT -> {
                    saveScore()
                    gameEngine.initGameEngine()
                }
                PauseActivity.QUIT_RESULT -> {
                    saveScore()
                    super.onBackPressed()
                }
            }
        }
    }

    private fun saveScore() {
        if (currentScore == 0)
            return

        val finalScore = Score(datetime = java.util.Date(), value = currentScore, fruits = currentScore/10)
        SqlManagement.getInstance(this).newScore(finalScore)
    }

    private fun setupUI() {
        setListener()
        initGestures()
    }

    private fun setListener() {
        overlayGroup.setOnClickListener {
            it.visibility = View.GONE
            table.visibility = View.VISIBLE
            gameEngine.run()
        }

        pause_btn.setOnClickListener {
            gameEngine.pause()
        }
    }

    private fun initGestures() {
        main_layout.setOnTouchListener(OnSwipeListener(this, object : OnSwipeListener.Listener {
            override fun onSwipeTop() {
                gameEngine.setDirection(Direction.UP)
            }

            override fun onSwipeBottom() {
                gameEngine.setDirection(Direction.DOWN)
            }

            override fun onSwipeLeft() {
                gameEngine.setDirection(Direction.LEFT)
            }

            override fun onSwipeRight() {
                gameEngine.setDirection(Direction.RIGHT)
            }
        }))
    }

    private fun initTableLayout() {
        val tableParams =
            TableLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT, TableLayout.LayoutParams.MATCH_PARENT, 1f)
        val rowParams =
            TableRow.LayoutParams(TableLayout.LayoutParams.WRAP_CONTENT, TableLayout.LayoutParams.WRAP_CONTENT, 1f)

        rowParams.marginStart = 5
        rowParams.marginEnd = 5
        rowParams.topMargin = 5
        rowParams.bottomMargin = 5

        for (i in 0 until GameEngine.SIZE_X) {
            val tableRow = TableRow(this)
            tableRow.layoutParams = tableParams
            for (j in 0 until GameEngine.SIZE_Y) {
                val img = AppCompatImageView(this)
                img.layoutParams = rowParams

                if (i == 10 && j == 10) {
                    img.setImageResource(R.drawable.ic_body_red)
                } else {
                    img.setImageResource(R.drawable.ic_body_white)
                }

                img.scaleType = ImageView.ScaleType.CENTER
                tableRow.addView(img)
            }
            table.addView(tableRow)
        }
    }

    // Interface implement

    override fun update(list: ArrayList<Box>, score: Int) {
        this.runOnUiThread {
            currentScore = score
            score_txt.text = "Score : $score"
            for (box in list) {
                val img = (table.getChildAt(box.x) as TableRow).getChildAt(box.y) as AppCompatImageView

                when (box.content) {
                    GridContent.SNAKE_HEAD -> img.setImageResource(R.drawable.ic_body_red)
                    GridContent.SNAKE_BODY -> img.setImageResource(R.drawable.ic_body_black)
                    GridContent.FRUIT -> img.setImageResource(R.drawable.ic_raspberry)
                    else -> img.setImageResource(R.drawable.ic_body_white)
                }
            }
        }
    }

    override fun pause() {
        isGameOver = false
        Intent(this, PauseActivity::class.java)
            .also { intent ->
                intent.putExtra(PauseActivity.SCORE, currentScore)
                intent.putExtra(PauseActivity.IS_GAME_OVER, isGameOver)
                startActivityForResult(intent, PAUSE_RESULT)
            }
    }

    override fun gameOver() {
        isGameOver = true
        Intent(this, PauseActivity::class.java)
            .also {intent ->
                intent.putExtra(PauseActivity.SCORE, currentScore)
                intent.putExtra(PauseActivity.IS_GAME_OVER, isGameOver)
                startActivityForResult(intent, PAUSE_RESULT)
            }
    }

    override fun initUI() {
        overlayGroup.visibility = View.VISIBLE
        table.visibility = View.GONE
        table.removeAllViews()
        initTableLayout()
        score_txt.text = "Score : 0"
    }
}
