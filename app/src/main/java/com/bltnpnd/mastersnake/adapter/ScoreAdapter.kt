package com.bltnpnd.mastersnake.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.bltnpnd.mastersnake.R
import com.bltnpnd.mastersnake.models.Score
import kotlinx.android.synthetic.main.item_score.view.*
import java.text.SimpleDateFormat

class ScoreAdapter(val context: Context, private var scoreListener: (Score) -> Unit) : RecyclerView.Adapter<ScoreAdapter.ViewHolder>() {

    private var scores: List<Score> = mutableListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_score, parent, false))
    }

    override fun getItemCount(): Int = scores.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val score = scores[position]

        with(holder) {
            positionTxt.text = (position + 1).toString()
            valueTxt.text = score.value.toString()
            dateTxt.text = SimpleDateFormat("dd/MM/yyyy").format(score.datetime)
            container.setOnClickListener { scoreListener.invoke(score) }
        }
    }

    fun fillScoresList(scores: List<Score>) {
        this.scores = scores
        notifyDataSetChanged()
    }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var container: View = view.score_container
        var positionTxt: TextView = view.position_textview
        var valueTxt: TextView = view.value_textview
        var dateTxt: TextView = view.date_textview
    }
}