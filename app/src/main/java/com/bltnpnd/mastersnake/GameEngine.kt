package com.bltnpnd.mastersnake

import android.util.Log
import com.bltnpnd.mastersnake.models.Direction
import com.bltnpnd.mastersnake.models.Grid
import com.bltnpnd.mastersnake.models.Snake
import com.bltnpnd.mastersnake.module.Fruit
import com.bltnpnd.mastersnake.module.Movement
import com.bltnpnd.mastersnake.utils.MouvementDirection
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlin.system.measureTimeMillis

class GameEngine(private val gameView: IGameView) {


    private var breakerLoop: Boolean = false // permet de casser la boucle de jeu
    private lateinit var snake: Snake
    private lateinit var grid: Grid
    private lateinit var direction: Direction

    companion object {
        const val SIZE_X = 20
        const val SIZE_Y = 20
        var SCORE = 0
    }

    init {
        Log.d("DEBUG", "Init Game Engine")
        initGameEngine()
    }

    fun initGameEngine() {
        grid = Grid(SIZE_X, SIZE_Y)
        snake = Snake(10, 10)
        direction = Direction.UP
        SCORE = 0
        gameView.initUI()
    }

    fun setDirection(direction: Direction) {
        if (!MouvementDirection.isOpposite(this.direction, direction))
            this.direction = direction
    }

    fun run() {
        breakerLoop = true
        Log.d("DEBUG", "START")
        GlobalScope.launch {
            loop()
        }
    }

    fun pause() {
        kill()
        gameView.pause()
    }

    fun resume() {
        run()
    }

    fun stop() {
        kill()
        gameView.gameOver()
    }

    fun kill() {
        breakerLoop = false
    }

    private fun loop() {
        while (breakerLoop) {
            val duration = measureTimeMillis {
                val newGrid = grid
                val dir = direction
                Log.d("DEBUG", "DIRECTION:$dir")

                Fruit.getInstance().fruitManagement(newGrid)
                callSubModuleMovement(newGrid, dir)

                gameView.update(grid.compare(newGrid), SCORE)
                grid = newGrid
            }

            Log.d("LOOP_DURATION", duration.toString())
            Thread.sleep(500 - duration)
        }
    }

    private fun callSubModuleMovement(newGrid: Grid, direction: Direction) {
        // WARNING !! en kotlin c'est une référence qui est passé et non une copie; pas besoin de retour
        // A confirmer avec le prof

        if (!when (direction) {
            Direction.UP -> Movement.getInstance().moveUp(newGrid, snake)
            Direction.DOWN -> Movement.getInstance().moveDown(newGrid, snake)
            Direction.LEFT -> Movement.getInstance().moveLeft(newGrid, snake)
            Direction.RIGHT -> Movement.getInstance().moveRight(newGrid, snake)
        }) stop()
    }
}