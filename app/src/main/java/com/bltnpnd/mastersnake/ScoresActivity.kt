package com.bltnpnd.mastersnake

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.widget.Toast
import com.bltnpnd.mastersnake.adapter.ScoreAdapter
import com.bltnpnd.mastersnake.models.Score
import com.bltnpnd.mastersnake.sqlite.SqlManagement
import kotlinx.android.synthetic.main.activity_scores.*
import java.util.*

class ScoresActivity : AppCompatActivity() {

    private lateinit var adapter: ScoreAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_scores)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        setTitle(R.string.home_scores)
        initAdapter()

        // TODO: Run one time to init database with 20 values
        //setupDatabase()

        getScores()
    }

    private fun initAdapter() {
        adapter = ScoreAdapter(this) { score -> Toast.makeText(this, "Click on score : " + score.value, Toast.LENGTH_SHORT).show() }
        scores_recyclerview.layoutManager = LinearLayoutManager(this)
        scores_recyclerview.adapter = adapter
    }

    private fun getScores() {
        adapter.fillScoresList(SqlManagement.getInstance(this).getScores())
    }

    private fun setupDatabase() {
        val dbManagement = SqlManagement.getInstance(this)

        for (i in 1..20) {
            val value = kotlin.random.Random.nextInt(0, 1000)
            val fruits = kotlin.random.Random.nextInt(0, 50)

            dbManagement.newScore(Score(Date(), value, fruits))
        }
    }
}
