package com.bltnpnd.mastersnake.sqlite

import android.content.Context
import com.bltnpnd.mastersnake.models.Score
import org.jetbrains.anko.db.*
import java.util.*

class SqlManagement (ctx: Context) {

    private val database: SqlHelper

    init {
        database = SqlHelper(ctx, DATABASE_NAME)
    }

    companion object {
        private var instance: SqlManagement? = null

        private val DATABASE_NAME = "mastersnakedb"

        fun getInstance(context: Context): SqlManagement {
            if (instance == null) {
                instance = SqlManagement(context)
            }

            return instance!!
        }
    }


    /**
     * Get list of best scores
     * By default get 10 best values
     */
    fun getScores(count: Int = 10): List<Score> = database.use {
        val scores = ArrayList<Score>()

        select("Score")
            .orderBy("value", SqlOrderDirection.DESC)
            .limit(count)
            .parseList(object: MapRowParser<List<Score>>{
                override fun parseRow(columns: Map<String, Any?>): List<Score> {

                    scores.add(Score(
                        Date(columns.getValue("datetime").toString().toLong()),
                        columns.getValue("value").toString().toInt(),
                        columns.getValue("fruits").toString().toInt()))

                    return scores
                }
            })

    scores
    }


    /**
     * Insert new score in database
     */
    fun newScore(score: Score) {
        database.use {
            insert("Score",
                "datetime" to score.datetime.time,
                "value" to score.value,
                "fruits" to score.fruits)
        }
    }


    /**
     * Delete all scores
     */
    fun cleanScores() {
        database.use {
            delete("Score")
        }
    }
}