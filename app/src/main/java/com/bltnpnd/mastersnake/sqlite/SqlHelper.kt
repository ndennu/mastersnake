package com.bltnpnd.mastersnake.sqlite

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import org.jetbrains.anko.db.*

class SqlHelper(ctx: Context, dbName: String) : ManagedSQLiteOpenHelper(ctx, dbName) {

    companion object {
        private var instance: SqlHelper? = null

        @Synchronized
        fun getInstance(ctx: Context, dbName: String): SqlHelper {
            if (instance == null) {
                instance = SqlHelper(ctx.applicationContext, dbName)
            }
            return instance!!
        }
    }

    override fun onCreate(db: SQLiteDatabase) {
        db.createTable("Score", true,
            "datetime" to INTEGER + PRIMARY_KEY,
            "value" to INTEGER,
            "fruits" to INTEGER)
    }

    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {}
}

// Access property for Context
val Context.database: SqlHelper
    get() = SqlHelper.getInstance(applicationContext, "mastersnakedb")