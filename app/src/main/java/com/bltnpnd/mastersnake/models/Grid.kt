package com.bltnpnd.mastersnake.models

class Grid(val width: Int, val height: Int) : Cloneable {

    private val boxList: ArrayList<Box> = ArrayList()

    init {
        for (i in 0 until width) {
            for (j in 0 until height) {
                boxList.add(Box(i, j))
            }
        }
    }

    fun updateBox(box: Box) {
        for (b in boxList) {
            if (b.compareTo(box) == 0) {
                b.content = box.content
                return
            }
        }
    }

    fun compare(grid: Grid): ArrayList<Box> {
        val result = ArrayList<Box>()

        for (box in boxList) {
            for (newBox in grid.boxList) {
                if (box.compareTo(newBox) == 0) {
                    result.add(newBox)
                    break
                }
            }
        }

        return result
    }

    fun containFruit(): Boolean {
        for (box in boxList) {
            if (box.content == GridContent.FRUIT) {
                return true
            }
        }
        return false
    }

    fun getEmptyBox(): ArrayList<Box> {
        val result = ArrayList<Box>()

        for (box in boxList) {
            if (box.content == GridContent.EMPTY)
                result.add(box)
        }
        return result
    }

    fun getBoxContentByCoordinate(x: Int, y: Int): GridContent {
        val box = Box(x, y)

        for (b in boxList) {
            if (b.compareTo(box) == 0)
                return b.content
        }
        return GridContent.UNDEFINED
    }

    fun updateSnake(snake: Snake, oldSnake: Snake) {
        updateSnakeWithContent(oldSnake, GridContent.EMPTY)
        updateSnakeWithContent(snake, GridContent.SNAKE_BODY)
    }

    private fun updateSnakeWithContent(snake: Snake, content: GridContent) {
        var tmpSnake: Snake? = snake
        if (content != GridContent.EMPTY) {
            tmpSnake = snake.next
            updateBox(Box(snake.x, snake.y, GridContent.SNAKE_HEAD))
        }

        while (tmpSnake != null) {
            updateBox(Box(tmpSnake.x, tmpSnake.y, content))
            tmpSnake = tmpSnake.next
        }
    }
}