package com.bltnpnd.mastersnake.models

import com.bltnpnd.mastersnake.utils.MouvementDirection


class Snake (var x: Int, var y: Int, var next: Snake? = null): Cloneable {

    fun addPart(direction: Direction): Snake {

        val oppositeDirection = MouvementDirection.getOpposite(direction)

        return if (next == null) {
            next = when (oppositeDirection) {
                Direction.UP -> Snake(x - 1, y)
                Direction.DOWN -> Snake(x + 1, y)
                Direction.LEFT -> Snake(x, y - 1)
                Direction.RIGHT -> Snake(x, y + 1)
            }

            next!!
        } else
            next!!.addPart(direction)
    }

    fun move(direction: Direction){
        var newX: Int = x
        var newY: Int = y

        when (direction) {
            Direction.UP -> newX -= 1
            Direction.DOWN -> newX += 1
            Direction.LEFT -> newY -= 1
            Direction.RIGHT -> newY += 1
        }

        moveSnake(x, y)
        x = newX
        y = newY
    }

    private fun moveSnake(newX: Int, newY: Int){
        if(next != null)
            next!!.moveSnake(x, y)

        x = newX
        y = newY
    }

    fun biteHimself(): Boolean {
        var n = next
        while (n != null) {
            if (x == n.x && y == n.y)
                return true
            n = n.next
        }
        return false
    }

    fun cloneSnake(): Snake {
        return clone()
    }

    override fun clone(): Snake {
        val result = Snake(x, y)

        if(next != null)
            result.next = next!!.clone()

        return result
    }
}