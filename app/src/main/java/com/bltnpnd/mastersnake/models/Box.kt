package com.bltnpnd.mastersnake.models

class Box(val x: Int, val y: Int, var content: GridContent = GridContent.EMPTY): Comparable<Box> {

    override fun compareTo(other: Box): Int {
        if(x == other.x && y == other.y)
            return 0

        return 1
    }

}