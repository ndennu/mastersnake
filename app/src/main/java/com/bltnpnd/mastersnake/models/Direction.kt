package com.bltnpnd.mastersnake.models

enum class Direction {
    UP,
    DOWN,
    LEFT,
    RIGHT
}