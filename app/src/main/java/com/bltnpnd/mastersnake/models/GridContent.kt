package com.bltnpnd.mastersnake.models

enum class GridContent {
    UNDEFINED,
    EMPTY,
    SNAKE_HEAD,
    SNAKE_BODY,
    FRUIT
}