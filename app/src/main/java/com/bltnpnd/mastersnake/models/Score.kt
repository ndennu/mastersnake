package com.bltnpnd.mastersnake.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize
import java.util.*

@Parcelize
data class Score (
    val datetime: Date,
    val value: Int,
    val fruits: Int
): Parcelable